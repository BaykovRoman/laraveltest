<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'auth:api'], function () {
	Route::post('test', 'Controller@test');

	Route::get('products/{id}', 'ProductController@show');
	Route::post('products', 'ProductController@create');
	Route::put('products/{id}', 'ProductController@update');
	Route::delete('products/{id}', 'ProductController@delete');
	Route::get('products/find/{searchParam}', 'ProductController@find');
	Route::post('products/filter/{page?}', 'ProductController@filter');

	Route::get('brands/{id}', 'BrandController@show');
	Route::post('brands', 'BrandController@create');
	Route::put('brands/{id}', 'BrandController@update');
	Route::delete('brands/{id}', 'BrandController@delete');

	Route::get('countries/{id}', 'CountryController@show');
	Route::post('countries', 'CountryController@create');
	Route::put('countries/{id}', 'CountryController@update');
	Route::delete('countries/{id}', 'CountryController@delete');

	Route::get('languages/{id}', 'LanguageController@show');
	Route::post('languages', 'LanguageController@create');
	Route::put('languages/{id}', 'LanguageController@update');
	Route::delete('languages/{id}', 'LanguageController@delete');

	Route::get('product-descriptions/{id}', 'ProductDescriptionController@show');
	Route::post('product-descriptions', 'ProductDescriptionController@create');
	Route::put('product-descriptions/{id}', 'ProductDescriptionController@update');
	Route::delete('product-descriptions/{id}', 'ProductDescriptionController@delete');

});