<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

	protected $fillable = [
		'sku',
		'price',
		'depth',
		'width',
		'height',
		'brand_id',
		'country_id',
		'weight',
		'barcode',
		'image',
		'quantity',
	];

	static $relationships = ['brand', 'country', 'descriptions'];

	public function brand()
	{
		return $this->belongsTo(Brand::class);
	}

	public function country()
	{
		return $this->belongsTo(Country::class);
	}

	public function descriptions()
	{
		return $this->hasMany(ProductDescription::class);
	}
}
