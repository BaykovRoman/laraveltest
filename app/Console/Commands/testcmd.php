<?php

namespace App\Console\Commands;

use App\Brand;
use App\Country;
use App\Product;
use function foo\func;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class testcmd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'testcmd';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

    	$filters = [
    		'country' => 'US',
			'brand' => 'Bradex',
		];

		/** @var \Illuminate\Database\Eloquent\Builder $builder */
		$builder = Product::with(Product::$relationships);

		foreach ($filters as $filter => $value) {
			switch ($filter) {
				case 'brand':
					$builder->whereHas('brand', function ($query) use ($value) {
						/** @var \Illuminate\Database\Eloquent\Builder $query */
						$query->where('name', 'like', '%' . $value . '%');
					});
					break;
				case 'name':
					$builder->whereHas('descriptions', function ($query) use ($value) {
						/** @var \Illuminate\Database\Eloquent\Builder $query */
						$query->where('name', 'like', '%' . $value . '%');
					});
					break;
				case 'description':
					$builder->whereHas('descriptions', function ($query) use ($value) {
						/** @var \Illuminate\Database\Eloquent\Builder $query */
						$query->where('description', 'like', '%' . $value . '%');
					});
					break;
				case 'sku':
					$builder->where('sku', 'like', '%' . $value . '%');
					break;
				case 'barcode':
					$builder->where('barcode', 'like', '%' . $value . '%');
					break;
				case 'min_price':
					$builder->where('price', '>', $value);
					break;
				case 'max_price':
					$builder->where('price', '<', $value);
					break;
				case 'min_depth':
					$builder->where('depth', '<', $value);
					break;
				case 'max_depth':
					$builder->where('depth', '>',$value);
					break;
				case 'min_width':
					$builder->where('width', '<', $value);
					break;
				case 'max_width':
					$builder->where('width', '>',$value);
					break;
				case 'min_height':
					$builder->where('height', '<', $value);
					break;
				case 'max_height':
					$builder->where('height', '>',$value);
					break;
				case 'min_weight':
					$builder->where('weight', '<', $value);
					break;
				case 'max_weight':
					$builder->where('weight', '>',$value);
					break;
				case 'country':
					$builder->whereHas('country', function ($query) use ($value) {
						/** @var \Illuminate\Database\Eloquent\Builder $query */
						$query->orWhere('code', 'like', '%' . $value . '%');
					});
					break;
			}
		}

//				echo $builder->getQuery()->toSql();die();
		echo $builder->count()." - \n";
		$products = $builder->take(10)->get();

		foreach ($products as $product) {
			echo $product->id.": ".$product->brand->id."\n";
		}
    	die();

    	$terms = ['Bradex', 'super', 'GA'];

		/** @var \Illuminate\Database\Eloquent\Builder $builder */

		$builder = Product::where(function ($query) use ($terms) {
			foreach ($terms as $term) {
				$query->orWhere('sku', 'like', '%' . $term . '%');
			}
		});

		$builder->orWhere(function ($query) use ($terms) {
			foreach ($terms as $term) {
				$query->orWhere('barcode', 'like', '%' . $term . '%');
			}
		});

		$builder->orWhereHas('brand', function ($query) use ($terms) {
			/** @var \Illuminate\Database\Eloquent\Builder $query */
			$query->where(function ($query) use ($terms) {
				foreach ($terms as $term) {
					$query->orWhere('name', 'like', '%' . $term . '%');
				}
			});
		});

		$builder->orWhereHas('descriptions', function ($query) use ($terms) {
			/** @var \Illuminate\Database\Eloquent\Builder $query */
			$query->where(function ($query) use ($terms) {
				foreach ($terms as $term) {
					$query->orWhere('name', 'like', '%' . $term . '%');
					$query->orWhere('description', 'like', '%' . $term . '%');
				}
			});
		});


		echo $builder->count()." - \n";
		$products = $builder->take(10)->get();



//		echo get_class($products);die();

//		echo $builder->getQuery()->toSql();die();

		foreach ($products as $product) {
			echo $product->id.": ".$product->brand->id."\n";
		}

		echo count($products);

    }
}
