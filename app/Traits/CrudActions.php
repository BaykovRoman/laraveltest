<?php

namespace App\Traits;

use Illuminate\Http\Request;

trait CrudActions
{
	/**
	 * @param Request $request
	 * @param $id
	 * @return mixed
	 */
	public function show(Request $request, $id)
	{
		/** @var \Illuminate\Database\Eloquent\Builder $builder */
		$object = $this->model::with($this->model::$relationships)->findOrFail($id);
		return $object;
	}

	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function create(Request $request)
	{
		return $this->model::create($request->all());
	}

	/**
	 * @param Request $request
	 * @param $id
	 * @return mixed
	 */
	public function update(Request $request, $id)
	{
		$object = $this->model::findOrFail($id);
		$object->update($request->all());
		return $object;
	}

	/**
	 * @param Request $request
	 * @param $id
	 * @return int
	 */
	public function delete(Request $request, $id)
	{
		$object = $this->model::findOrFail($id);
		$object->delete();
		return 204;
	}
}