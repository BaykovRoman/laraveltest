<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
	protected $fillable = [
		'name',
	];

	static $relationships = [];

	public function product()
	{
		return $this->hasMany(Product::class);
	}
}
