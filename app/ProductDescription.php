<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDescription extends Model
{
	protected $fillable = [
		'name',
		'description',
		'product_id',
		'language_id',
	];

	static $relationships = [];

	public function product()
	{
		$this->belongsTo(Product::class);
	}
}
