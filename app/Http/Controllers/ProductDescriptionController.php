<?php

namespace App\Http\Controllers;

use App\ProductDescription;
use App\Traits\CrudActions;


class ProductDescriptionController extends Controller
{
	use CrudActions;

	private $model = ProductDescription::class;
}