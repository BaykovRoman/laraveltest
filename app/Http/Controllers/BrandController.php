<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Traits\CrudActions;

class BrandController extends Controller
{
	use CrudActions;

	private $model = Brand::class;
}