<?php

namespace App\Http\Controllers;

use App\Language;
use App\Traits\CrudActions;

class LanguageController extends Controller
{
	use CrudActions;

	private $model = Language::class;
}