<?php

namespace App\Http\Controllers;

use App\Country;
use App\Traits\CrudActions;

class CountryController extends Controller
{
	use CrudActions;

	private $model = Country::class;
}