<?php

namespace App\Http\Controllers;

use App\Product;
use App\Traits\CrudActions;
use Illuminate\Http\Request;

/**
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
	use CrudActions;

	private $model = Product::class;

	/**
	 * @param Request $request
	 * @param $searchParam
	 * @return array
	 */
	public function find(Request $request, $searchParam)
	{
		$searchTerms = explode(",", $searchParam);

		/** @var \Illuminate\Database\Eloquent\Builder $builder */
		$builder = Product::with(['brand', 'country', 'descriptions']);
		$builder->where(function ($query) use ($searchTerms) {
			foreach ($searchTerms as $term) {
				$query->orWhere('sku', 'like', '%' . $term . '%');
			}
		});

		$builder->orWhere(function ($query) use ($searchTerms) {
			foreach ($searchTerms as $term) {
				$query->orWhere('barcode', 'like', '%' . $term . '%');
			}
		});

		$builder->orWhereHas('brand', function ($query) use ($searchTerms) {
			/** @var \Illuminate\Database\Eloquent\Builder $query */
			$query->where(function ($query) use ($searchTerms) {
				foreach ($searchTerms as $term) {
					$query->orWhere('name', 'like', '%' . $term . '%');
				}
			});
		});

		$builder->orWhereHas('country', function ($query) use ($searchTerms) {
			/** @var \Illuminate\Database\Eloquent\Builder $query */
			$query->where(function ($query) use ($searchTerms) {
				foreach ($searchTerms as $term) {
					$query->orWhere('code', 'like', '%' . $term . '%');
				}
			});
		});

		$builder->orWhereHas('descriptions', function ($query) use ($searchTerms) {
			/** @var \Illuminate\Database\Eloquent\Builder $query */
			$query->where(function ($query) use ($searchTerms) {
				foreach ($searchTerms as $term) {
					$query->orWhere('name', 'like', '%' . $term . '%');
					$query->orWhere('description', 'like', '%' . $term . '%');
				}
			});
		});

//		$totalResults = $builder->count();
		return $builder->paginate(10);
//		return [
//			'products' => $products,
//			'total' => $totalResults,
//		];
	}


	/**
	 * @param Request $request
	 * @param int $page
	 * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
	 */
	public function filter(Request $request, $page = 1)
	{
		/** @var \Illuminate\Database\Eloquent\Builder $builder */
		$builder = Product::with(Product::$relationships);

		foreach ($request->all() as $filter => $value) {
			switch ($filter) {
				case 'brand':
					$builder->whereHas('brand', function ($query) use ($value) {
						/** @var \Illuminate\Database\Eloquent\Builder $query */
						$query->where('name', 'like', '%' . $value . '%');
					});
					break;
				case 'name':
					$builder->whereHas('descriptions', function ($query) use ($value) {
						/** @var \Illuminate\Database\Eloquent\Builder $query */
						$query->where('name', 'like', '%' . $value . '%');
					});
					break;
				case 'description':
					$builder->whereHas('descriptions', function ($query) use ($value) {
						/** @var \Illuminate\Database\Eloquent\Builder $query */
						$query->where('description', 'like', '%' . $value . '%');
					});
					break;
				case 'sku':
					$builder->where('sku', 'like', '%' . $value . '%');
					break;
				case 'barcode':
					$builder->where('barcode', 'like', '%' . $value . '%');
					break;
				case 'min_price':
					$builder->where('price', '>', $value);
					break;
				case 'max_price':
					$builder->where('price', '<', $value);
					break;
				case 'min_depth':
					$builder->where('depth', '<', $value);
					break;
				case 'max_depth':
					$builder->where('depth', '>', $value);
					break;
				case 'min_width':
					$builder->where('width', '<', $value);
					break;
				case 'max_width':
					$builder->where('width', '>', $value);
					break;
				case 'min_height':
					$builder->where('height', '<', $value);
					break;
				case 'max_height':
					$builder->where('height', '>', $value);
					break;
				case 'min_weight':
					$builder->where('weight', '<', $value);
					break;
				case 'max_weight':
					$builder->where('weight', '>', $value);
					break;
				case 'country':
					$builder->whereHas('country', function ($query) use ($value) {
						/** @var \Illuminate\Database\Eloquent\Builder $query */
						$query->where('code', 'like', '%' . $value . '%');
					});
					break;
			}
		}
		//			$totalResults = $builder->count();
		return $builder->paginate(10);
	}
}