<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	protected $fillable = [
		'code',
	];

	static $relationships = [];

	public function product()
	{
		return $this->belongsTo(Product::class);
	}
}
