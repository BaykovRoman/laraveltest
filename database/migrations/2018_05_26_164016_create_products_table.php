<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('products', function (Blueprint $table) {
			$table->increments('id');
			$table->string('sku', 20)->nullable();
			$table->float('price')->nullable();
			$table->float('depth', 255)->nullable();
			$table->float('width', 255)->nullable();
			$table->float('height', 255)->nullable();
			$table->integer('brand_id', false, true)->nullable();
			$table->integer('country_id', false, true)->nullable();
			$table->float('weight')->nullable();
			$table->string('barcode', 128)->nullable();
			$table->string('image', 255)->nullable();
			$table->integer('quantity', false, true)->nullable();
			$table->timestamps();

			$table->index('brand_id');
			$table->index('country_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
