<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('products', function (Blueprint $table) {
			$table->foreign('brand_id')
				->references('id')
				->on('brands')
				->onDelete('set null')
				->onUpdate('cascade');
			$table->foreign('country_id')
				->references('id')
				->on('countries')
				->onDelete('set null')
				->onUpdate('cascade');
		});
		Schema::table('product_descriptions', function (Blueprint $table) {
			$table->foreign('product_id')
				->references('id')
				->on('products')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->foreign('language_id')
				->references('id')
				->on('languages')
				->onDelete('cascade')
				->onUpdate('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
	public function down()
	{
		Schema::table('products', function (Blueprint $table) {
			$table->dropForeign(['brand_id']);
			$table->dropForeign(['country_id']);
		});
		Schema::table('product_descriptions', function (Blueprint $table) {
			$table->dropForeign(['product_id']);
			$table->dropForeign(['language_id']);
		});
	}
}
