<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		\App\Language::firstOrNew(['name' => 'Русский'])->save();
		\App\Language::firstOrNew(['name' => 'English'])->save();

		\App\Brand::firstOrNew(['name' => 'Chicony'])->save();
		\App\Brand::firstOrNew(['name' => 'Olymp GmbH'])->save();
		\App\Brand::firstOrNew(['name' => 'Bradex'])->save();
		\App\Brand::firstOrNew(['name' => 'Health Hoop'])->save();
		\App\Brand::firstOrNew(['name' => 'BMD GmbH'])->save();
		\App\Brand::firstOrNew(['name' => 'Syton'])->save();
		\App\Brand::firstOrNew(['name' => 'REDMOND'])->save();
		\App\Brand::firstOrNew(['name' => 'Polaris'])->save();


		factory(App\Country::class, 20)->create()->each(function ($country) {
			$country->save();
		});

		$popProductsIterations = 10;
		$languages = \App\Language::all();
		for ($index = 1; $index <= $popProductsIterations; $index++) {
			echo "Populating products " . $index . "/" . $popProductsIterations . "\n";
			factory(App\Product::class, 10000)->create()->each(function ($product) use ($languages) {
				$descriptions = [];
				foreach ($languages as $language) {
					$descriptions[] = factory(App\ProductDescription::class)->make([
						'language_id' => $language->id,
					]);
				}
				$product->descriptions()->saveMany($descriptions);
			});
		}

	}
}
