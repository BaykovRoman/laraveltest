<?php

use Faker\Generator as Faker;

$factory->define(App\ProductDescription::class, function (Faker $faker) {
	return [
		'name' => $faker->text(50),
		'description' => $faker->text(),
	];
});