<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
	return [
		'sku' => $faker->numerify('07######'),
		'price' => $faker->randomFloat(2, 0, 500),
		'depth' => $faker->randomFloat(2, 1, 100),
		'width' => $faker->randomFloat(2, 1, 100),
		'height' => $faker->randomFloat(2, 1, 100),
		'brand_id' => \App\Brand::all()->random()->id,
		'country_id' => \App\Country::all()->random()->id,
		'weight' => $faker->randomFloat(2, 0, 500),
		'barcode' => $faker->numerify('############'),
		'image' => $faker->imageUrl(),
		'quantity' => $faker->randomNumber(),
	];
});